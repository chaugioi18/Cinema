<%-- 
    Document   : bill
    Created on : Nov 9, 2016, 11:06:48 PM
    Author     : vfi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bill Page</title>
    </head>
    <%@include file="../header.jsp" %>
    <%@include file="../slide.jsp" %>
    <body>      
        <div class="container">
            <form>
                <h2>Bill Details</h2>
                <div class="form-group">
                    <label for="cinema">Cinema: </label>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>                        
                            <th>Film</th>
                            <th>Room</th>
                            <th>Price</th>
                            <th>Sale percents</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Seat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Tom and Jerry</td>
                            <td>Room 3</td>
                            <td>90.000</td>
                            <td>0%</td>
                            <td>25/12/2016</td>
                            <td>11h15</td>
                            <td>A17</td>
                        </tr>                        
                    </tbody>
                </table>
                <div class="form-group">
                    <label for="total">Total: 50.000</label>                
                </div>
                <div class="form-group">
                    <label>Your ticket code was sent to your email nguyenngocchau18@gmail.com.</label>                
                </div>
                <div class="form-group">
                    <input type="submit" value="Done" class="btn btn-success"/>
                </div>
            </form>
        </div>

    </body>
    <%@include file="../footer.jsp" %>
</html>
