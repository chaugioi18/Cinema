<%-- 
    Document   : payment
    Created on : Nov 9, 2016, 11:05:32 PM
    Author     : vfi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment Page</title>
    </head>
    <%@include file="../header.jsp" %>
    <%@include file="../slide.jsp" %>
    <body>
        <div class="container">
        <form>
            <div class="form-group">
                <label for="creditcard">Credit Card No.</label>
                <input class="form-control" id ="creditcard"/>
            </div>
            <div class="form-group">
                <label for="month">Month</label>    
                <input class="form-control" id ="month"/>
            </div>
            <div class="form-group">
                <label for="year">Year</label>    
                <input class="form-control" id ="year"/>
            </div>
            <div class="form-group">
                <label for="email">Email</label>    
                <input class="form-control" id ="email"/>
            </div>
            <div class="form-group">
                <label for="name">Name</label>    
                <input class="form-control" id ="name"/>
            </div>
            <input class="btn btn-success" type="submit" value="Payment"/>
        </form>
            </div>
    </body>
    <%@include file="../footer.jsp" %>
</html>
