<%-- 
    Document   : index
    Created on : Nov 8, 2016, 12:22:15 PM
    Author     : vfi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>Home Page</title>
    </head>
    <%@include file="../header.jsp" %>
    <%@include file="../slide.jsp" %>
    <body>
        <div class="container">
        <form>
            <div class="form-group">
                <label for="film">Film</label>
                <select class="form-control" id="film">
                    <option></option>
                </select>    
            </div>
            <div class="form-group">
                <label for="cinema">Cinema</label>    
                <select class="form-control" id="cinema">
                    <option></option>
                </select>
            </div>
            <div class="form-group">
                <label for="date">Date</label>    
                <select class="form-control" id="date">
                    <option></option>
                </select>
            </div>
            <div class="form-group">
                <label for="showtime">Timeline</label>    
                <select class="form-control" id="showtime">
                    <option></option>
                </select>
            </div> 
            <input class="btn btn-success" type="submit" value="Buy"/>
        </form>
        </div>
    </body>
    <%@include file="../footer.jsp" %>
</html>
