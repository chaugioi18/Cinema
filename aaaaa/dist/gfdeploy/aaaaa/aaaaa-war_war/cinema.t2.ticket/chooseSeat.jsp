<%-- 
    Document   : abc
    Created on : Nov 8, 2016, 12:28:37 PM
    Author     : vfi
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Choose seat Page</title>
    </head>
    <%@include file="../header.jsp" %>
    <%@include file="../slide.jsp" %>
    <body>
        <div class="container">
            <form class="container">
                <div class="form-group">
                    <label for="quantity">Quantity</label>
                    <input type="number" id="quantity"/>
                </div>
                <div>
                    <table class="table borderless">
                    <thead>
                        <tr>     
                            <th></
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                            <th>8</th>
                            <th>9</th>
                            <th>10</th>
                        </tr>
                    </thead>
                    <tbody>                        
                            <tr>
                                <td>A</td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                            </tr>                        
                            
                            <tr>
                                <td>B</td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                            </tr>
                            
                            <tr>
                                <td>C</td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                            </tr>
                            
                            <tr>
                                <td>D</td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                            </tr>
                            
                            <tr>
                                <td>E</td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                                <td><img src="${pageContext.request.contextPath}/cinema.t2.ticket/images/cinemachair.jpg" height="40" width="40"/></td>
                            </tr>
                    </tbody>
                        
                        </table>
                </div>
                <div class="form-group">
                    <input type="submit" value="Payment"/>
                </div>
            </form>
        </div>
    </body>
    <%@include file="../footer.jsp" %>
</html>
